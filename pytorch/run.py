import os
import time
import pickle
import logging
import argparse

import torch
import numpy as np

from model import IMDBNet
from dataset import IMDBDataset


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('data', metavar='DIR',
                        help="Path to training and test data")
    parser.add_argument('out_fname',
                        help="Results and configs will be stored here")
    parser.add_argument('--num-epochs', default=1, type=int,
                        help="")
    parser.add_argument('--batch-size', default=256, type=int,
                        help="")
    parser.add_argument('--num-workers', default=0, type=int,
                        help="Number of subprocesses to use for loading data into device memory")
    parser.add_argument('--learning-rate', default=1e-3, type=float,
                        help="Learning rate for training the model")
    parser.add_argument('--adjust-lr', default=False, action='store_true',
                        help="Determines whether to use a learning rate schedule. Time based LR schedule used.")
    parser.add_argument('--optimizer', type=str, default='adam',
                        help="Optimizer to use for training. Options are 'sgd' and 'adam'.")
    parser.add_argument('--num-gpus', type=int, default=0,
                        help="Pass this flag to specify how many GPUs to use. 0 means the CPU should be used.")
    parser.add_argument('--use-sparse', action='store_true', default=False,
                        help="Determines whether or not to use sparse embeddings and optimizers")

    args = parser.parse_args()

    # Logging set up
    logger = logging.getLogger('PyTorch')
    logger.setLevel(logging.DEBUG)
    fh = logging.FileHandler('info.log')
    fh.setLevel(logging.DEBUG)
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
    fh.setFormatter(formatter)
    ch.setFormatter(formatter)
    logger.addHandler(fh)
    logger.addHandler(ch)

    # Check for gpus
    use_gpu = (args.num_gpus > 0) and (torch.cuda.is_available())
    if use_gpu:
        logger.info("Using GPU(s)")
    else:
        logger.info("Using CPU(s)")

    logger.info("Loading Datasets")
    train_data = IMDBDataset(os.path.join(args.data, 'train_data.pkl'))
    test_data = IMDBDataset(os.path.join(args.data, 'test_data.pkl'))

    if use_gpu:
        train_loader = torch.utils.data.DataLoader(
                            train_data,
                            batch_size = args.batch_size,
                            shuffle = True,
                            num_workers = args.num_workers,
                            pin_memory=True)
    else:
        train_loader = torch.utils.data.DataLoader(
                            train_data,
                            batch_size = args.batch_size,
                            shuffle = True,
                            num_workers = args.num_workers)

    test_loader = torch.utils.data.DataLoader(
                        test_data,
                        batch_size = args.batch_size,
                        shuffle = False,
                        num_workers = args.num_workers)

    embedding_dim = 32
    hidden_dim = 100
    nClasses = 2
    model = IMDBNet(embedding_dim, train_data.vocab_size, hidden_dim, nClasses, 
                    sparse_embeddings=args.use_sparse)

    loss_function = torch.nn.CrossEntropyLoss()
    if use_gpu:
        loss_function = loss_function.cuda()

    if use_gpu:
        # Setting device ids this way is necessary due to bug in PyTorch (or feature?) where cuda devices are 0-indexed,
        # instead of actual device ids output by nvidia-smi
        device_ids = [i for i in range(args.num_gpus)]
        model = torch.nn.DataParallel(model, device_ids)
        model.cuda()
        loss_function.cuda()

    if args.optimizer == 'sgd':
        dense_optimizer = torch.optim.SGD(model.parameters(), lr=args.learning_rate)
        if args.use_sparse:
            sparse_optimizer = None
    elif args.optimizer == 'adam':
        if args.use_sparse:
            param_list = [x for x in model.parameters()]
            dense_optimizer = torch.optim.Adam(param_list[1:], lr=args.learning_rate)
            sparse_optimizer = torch.optim.SparseAdam([param_list[0]], lr=args.learning_rate)
        else:
            dense_optimizer = torch.optim.Adam(model.parameters(), lr=args.learning_rate)


    optimizers = [dense_optimizer]
    #if sparse_optimizer is not None:
    if args.use_sparse:
        optimizers.append(sparse_optimizer)

    if args.adjust_lr:
        schedulers = []
        for optimizer in optimizers:
            for param_group in optimizer.param_groups:
                param_group['lr'] = args.learning_rate
            scheduler = torch.optim.lr_scheduler.StepLR(optimizer, step_size=10, gamma=0.1)
            schedulers.append(scheduler)

    # Stores all losses over the range of epochs --> used to show convergence rate
    training_losses = []
    training_times = []

    logger.info("Beginning Training and evaluation")
    for epoch in range(args.num_epochs):
        # Run the training phase for this epoch, collect loss and accuracy
        start = time.time()
        train_loss, train_acc = train(train_loader, model, loss_function, optimizers, epoch, use_gpu)
        end = time.time()

        # Not considering scheduler step time in benchmark time
        if args.adjust_lr:
            for scheduler in schedulers:
                scheduler.step()

        training_losses.append(train_loss)
        training_times.append(end-start)

        # Run the validation phase
        test_acc = validate(test_loader, model, epoch, use_gpu)

        logger.info("[Epoch: {}]\t [Time: {:.2f}s]\t [Avg loss: {:.5f}]\t [Avg Train acc: {:.3f}%]\t "
            "[Avg Test acc: {:.3f}%]".format(epoch, end-start, train_loss, train_acc, test_acc))


    avg_time_per_epoch = np.array(training_times, dtype=np.float).sum() / args.num_epochs
    logger.info("-------------------- Average time/epoch: [{:.3f}s] -----------------------".format(avg_time_per_epoch))

    # save results and conigs
    save_data = {'num_epochs': args.num_epochs,
                'batch_size': args.batch_size,
                'num_workers': args.num_workers,
                'training_losses': training_losses,
                'training_times': training_times,
                'optimizer': args.optimizer,
                'num_gpus': torch.cuda.device_count(),
                'avg_time_per_epoch': avg_time_per_epoch,}

    with open(args.out_fname, 'wb') as fout:
        pickle.dump(save_data, fout)


def train(train_loader, model, loss_function, optimizers, epoch, use_gpu):
    """
    Runs the forward and backward passes on the provided model.

    Parameters
    ----------
    train_loader: torch.utils.data.DataLoader
        Data loader object that co-ordinates batching of training set.
    model: torch.nn.Model
        The model to be trained
    loss_function: torch.nn.modules.loss
        Criterion used in training.
    optimizer: [torch.optim]
        List of Optimizer objects used in training. Note that this is a list to allow a mixture of Sparse and
        Dense optmizers, since PyTorch does not yet have an optimizer that handles both sparse and dense gradients
    epoch: int
        Current epoch being trained
    use_gpu: bool
        If True, GPUs will be used for training
    # TODO: don't measure time in every batch, it reduces performance
    """

    # switch to train mode
    model.train()

    total_loss = 0.0
    total_acc = 0.0
    num_instances = 0
    num_batches = 0
    for i, (input, target) in enumerate(train_loader):
        num_batches += 1
        if use_gpu:
            # Note: there are warnings about not using inputs.cuda() in alongside DataParallel. These have
            # empirically resulted in training errors.
            input = torch.autograd.Variable(input)

            # Setting async=True allows copying of labels to the GPU asynchronously, since they're not needed until
            # after the forward pass. This frees up
            target = target.cuda(async=True)
            target = torch.autograd.Variable(target)
        else:
            input = torch.autograd.Variable(input)
            target = torch.autograd.Variable(target)


        output = model(input)
        loss = loss_function(output, target.view(-1))

        for optimizer in optimizers:
            optimizer.zero_grad()
        loss.backward()
        for optimizer in optimizers:
            optimizer.step()

        total_loss += loss.data[0]

        _, pred = output.topk(1)
        total_acc += accuracy(pred, target)

    epoch_loss = total_loss / num_batches
    epoch_acc = total_acc / num_batches
    return epoch_loss, epoch_acc


def validate(test_loader, model, epoch, use_gpu):
    """
    Evaluates the model on data provided by test_loader.

    Parameters
    ----------
    test_loader: torch.utils.data.DataLoader
        Provides iterator on batched test data
    model: torch.nn.Model
        Model to compute forward pass on
    epoch: int
        The current epoch being rung
    use_gpu: bool
        Determines if the GPU should be used
    """

    # Switch to evaluation mode
    model.eval()
    total_acc = 0.0
    num_batches = 0
    for i, (input, target) in enumerate(test_loader):
        num_batches += 1
        if use_gpu:
            input = torch.autograd.Variable(input)

            target = target.cuda(async=True)
            target = torch.autograd.Variable(target)

        else:
            input = torch.autograd.Variable(input)
            target = torch.autograd.Variable(target)

        output = model(input)
        _, pred = output.topk(1)
        total_acc += accuracy(pred, target)

    return total_acc / num_batches


def accuracy(output, target):
    batch_size = output.data.shape[0]
    correct = output.eq(target).float().sum()
    accuracy = correct.mul_(100.0 / batch_size)
    return accuracy.data[0]


if __name__ == "__main__":
    main()

