import pytest
import torch
from run import accuracy

def test_accuracy():
    output = torch.IntTensor([1, 0, 1, 1])
    target = torch.IntTensor([0, 0, 1, 0])
    output = torch.autograd.Variable(output)
    target = torch.autograd.Variable(target)
    batch_size = output.data.shape[0]

    res = accuracy(output, target).data
    assert (res == 2 * (100.0 / batch_size)).all()
