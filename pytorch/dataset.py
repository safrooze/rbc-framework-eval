import pickle

import torch
from torch.utils.data import Dataset


class IMDBDataset(torch.utils.data.Dataset):
    """
    Creates a PyTorch Dataset object that is readable by a DatasetLoader object.

    Parameters
    ----------
    data_path: str, required
        The path to the data for which a Dataset object is being created. Could be train or test data.
    sequence_length: int, optional
        Size of the sentence length for each review. Defaults to 500.
    """

    def __init__(self, data_path: str, sequence_length=500):
        with open(data_path, 'rb') as fin:
            self.inputs, self.labels, self.vocab_size = pickle.load(fin)
        self.seq_len = sequence_length

    def __len__(self):
        return len(self.inputs)

    def __getitem__(self, index: int):
        seq = torch.from_numpy(self.inputs[index]).type(torch.LongTensor)
        label = torch.LongTensor([self.labels[index].tolist()])
        return seq, label
