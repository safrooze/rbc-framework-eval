from mxnet import gluon
import mxnet.ndarray as nd

class IMDBNet(gluon.Block):
    def __init__(self, embedding_dim, vocab_size, hidden_dim, nClasses):
        super(IMDBNet, self).__init__()

        with self.name_scope():
            self.hidden_dim = hidden_dim
            self.embedding = gluon.nn.Embedding(vocab_size, embedding_dim)

            # T=sequence_length, N=batch_size, C=feature dimension
            self.lstm = gluon.rnn.LSTM(hidden_dim, input_size=embedding_dim, layout='NTC')

            #self.linear = gluon.nn.Dense(nClasses, in_units=hidden_dim, activation='sigmoid')
            self.linear = gluon.nn.Dense(nClasses, in_units=hidden_dim)
            
    def forward(self, x):
        embeds = self.embedding(x)
        lstm_out = self.lstm(embeds)
        mean_lstm = nd.mean(lstm_out, 1)
        logits = self.linear(mean_lstm)
        return logits
