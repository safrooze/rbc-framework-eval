import os
import time
import pickle
import logging
import argparse

import numpy as np

from model import IMDBNet
from dataset import IMDBDataset

import mxnet as mx
from mxnet import gluon
from mxnet import autograd
import mxnet.ndarray as nd

# TODO: Need to make sure I'm annotating variable types in the functions

def main():
    """
    Using terminology from pytorch to stay consistent
    NOTE: Default behaviour is to use CPUs! Set the --num-gpus flag to the number of gpus available to set gpus
    to be used.
    """

    # Command line arguments set up
    parser = argparse.ArgumentParser()
    parser.add_argument('data', metavar='DIR',
                        help="Path to training and test data")
    parser.add_argument('out_fname',
                        help="Results and configs will be stored here")
    parser.add_argument('--num-epochs', default=1, type=int,
                        help="")
    parser.add_argument('--batch-size', default=256, type=int,
                        help="")
    parser.add_argument('--num-workers', default=0, type=int,
                        help="Number of subprocesses to use for loading data to device memory")
    parser.add_argument('--learning-rate', default=1e-3, type=float,
                        help="Learning rate for training the model")
    parser.add_argument('--adjust-lr', default=False, action='store_true',
                        help="Determines whether to use a learning rate schedule. Time based LR schedule used.")
    parser.add_argument('--optimizer', type=str, default='adam',
                        help="Optimizer to use for training. Options are 'sgd' and 'adam'.")
    parser.add_argument('--num-gpus', type=int, default=0,
                        help="Pass this flag to specify how many GPUs to use. 0 means the GPU should be used.")

    args = parser.parse_args()

    # Logging set up
    logger = logging.getLogger('MXNet')
    logger.setLevel(logging.DEBUG)
    fh = logging.FileHandler('info.log')
    fh.setLevel(logging.DEBUG)
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
    fh.setFormatter(formatter)
    ch.setFormatter(formatter)
    logger.addHandler(fh)
    logger.addHandler(ch)

    # Check for gpus
    use_gpu = args.num_gpus != 0
    if use_gpu:
        #logger.info("Using GPUs with device ids: {}".format(os.environ['CUDA_VISIBLE_DEVICES']))
        logger.info("Using GPUs")
        context = [mx.gpu(i) for i in range(args.num_gpus)]
    else:
        logger.info("GPUs unavailable, using CPU")
        context = [mx.cpu(0)]

    logger.info("Loading Datasets")
    train_data = IMDBDataset(os.path.join(args.data, 'train_data.pkl'))
    test_data = IMDBDataset(os.path.join(args.data, 'test_data.pkl'))

    train_loader = gluon.data.DataLoader(train_data,
                              batch_size = args.batch_size,
                              shuffle = True,
                              num_workers = args.num_workers)

    test_loader = gluon.data.DataLoader(test_data,
                              batch_size = args.batch_size,
                              shuffle = False,
                              num_workers = args.num_workers)

    embedding_dim = 32
    hidden_dim = 100
    nClasses = 2
    model = IMDBNet(embedding_dim, train_data.vocab_size, hidden_dim, nClasses)
    model.collect_params().initialize(mx.init.Xavier(rnd_type='gaussian', magnitude=2), force_reinit=True, ctx=context)

    loss_function = gluon.loss.SoftmaxCrossEntropyLoss()

    optimizer_params = {'learning_rate': args.learning_rate}
    if args.adjust_lr:
        # Unlike PyTorch, in MXNet, the step size input to the scheduler is number of trainer.step() calls, not num 
        # epochs. This is because PyTorch keeps track of num epochs internally, and updates it every time 
        # scheduler.step() is called. In Gluon the scheduler is tied to the Trainer object. 
        step_size = 10 * len(train_loader)
        optimizer_params['lr_scheduler'] = mx.lr_scheduler.FactorScheduler(step=step_size, factor=0.1)
    optimizer = gluon.Trainer(model.collect_params(), args.optimizer, optimizer_params)

    training_losses = []
    training_times = []
    logger.info("Beginning Training and evaluation")
    for epoch in range(args.num_epochs):
        start = time.time()
        train_loss, train_acc = train(train_loader, model, loss_function, optimizer, epoch, context, args.batch_size)
        end = time.time()
        training_losses.append(train_loss)
        training_times.append(end-start)

        test_acc = validate(test_loader, model, epoch, context)

        logger.info("[Epoch: {}]\t [Time: {:.2f}s]\t [Avg loss: {:.5f}]\t [Avg Train acc: {:.3f}%]\t "
                    "[Avg Test acc: {:.3f}%]".format(epoch, end-start, train_loss, train_acc, test_acc))

    # Note: not using time in first epoch in measurement because it fluctuates
    avg_time_per_epoch = nd.sum(nd.array(training_times[1:])) / (args.num_epochs - 1)
    logger.info("-------------------- Average time/epoch: [{:.3f}s] -----------------------".format(
        avg_time_per_epoch.asscalar()))

    # save results and configs
    save_data = {'num_epochs': args.num_epochs,
                'batch_size': args.batch_size,
                'training_losses': training_losses,
                'training_times': training_times,
                'optimizer': args.optimizer,
                'num_gpus': args.num_gpus,
                'avg_time_per_epoch': avg_time_per_epoch.asscalar(),}

    with open(args.out_fname, 'wb') as fout:
        pickle.dump(save_data, fout)


def train(train_loader, model, loss_function, optimizer, epoch, context, batch_size):
    """

    """

    total_loss = 0.0
    total_acc = 0.0
    num_batches = 0

    for i, (input, target) in enumerate(train_loader):
        num_batches += 1

        input = gluon.utils.split_and_load(input, context)
        target = gluon.utils.split_and_load(target, context)

        # losses is a list where each element is loss for device in context
        losses = []
        for data, label in zip(input, target):
            with autograd.record():
                output = model(data)
                loss = loss_function(output, label)
            loss.backward()
            total_loss += nd.mean(loss).asscalar()

            pred = nd.topk(output, k=1)
            total_acc += accuracy(pred, label)

        optimizer.step(batch_size)


    # need to further divide total loss and accuracy by number of gpus since losses were computed
    # per device
    num_gpus = len(context)
    train_loss = total_loss / (num_batches * num_gpus)
    train_acc = total_acc / (num_batches * num_gpus)
    return train_loss, train_acc

def validate(test_loader, model, epoch, context):
    """

    """

    total_acc = 0.0
    num_batches = 0
    for i, (input, target) in enumerate(test_loader):
        num_batches += 1

        input = gluon.utils.split_and_load(input, context)
        target = gluon.utils.split_and_load(target, context)

        for data, label in zip(input, target):
            output = model(data)
            pred = nd.topk(output, k=1)
            total_acc += accuracy(pred, label)

    num_gpus = len(context)
    return total_acc / (num_batches * num_gpus)

def accuracy(output, target):
    batch_size = output.shape[0]
    correct = nd.sum(output.__eq__(target))
    accuracy = correct.__mul__(100.0 / batch_size)
    return accuracy.asscalar()

if __name__ == "__main__":
    main()
